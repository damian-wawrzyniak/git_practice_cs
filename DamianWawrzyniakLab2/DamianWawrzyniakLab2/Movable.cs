﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamianWawrzyniakLab2
{
    interface Movable
    {
        void Start();
        void MoveForward(int HowFar);
        void MoveBackward(int HowFar);
        void MoveRight(int HowFar);
        void MoveLeft(int HowFar);
        void SpeedUp(int HowMuch);
        void SlowDown(int HowMuch);
        void Stop();

    }
}
