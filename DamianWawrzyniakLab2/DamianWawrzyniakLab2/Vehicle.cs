﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DamianWawrzyniakLab2
{
    public class Vehicle : Base, Movable
    {      
        public int MaxSpeed { get; set; }
        public Owner VehicleOwner { get; set; }
        public int ActualSpeed { get; set; }
        private string modelOfVehicle;
        protected int x; // współrzędna x poruszającego się pojazdu
        private int startingX;
        protected int y; // współrzędna y poruszającego się pojazdu
        private int startingY;
        protected bool AwailableToMove;
        public double DistanceCounter { get; set; }
        protected double time;

        public Vehicle(string vehicleModel)
        {
            modelOfVehicle = vehicleModel;
        }

        protected void TimeControl(double distance, double speed)
        {
            time += (distance / speed);
        }

        public virtual void ChangeOwnerOfVehicle(Owner newOwner, int prize)
        {
            VehicleOwner = newOwner;
            newOwner.AmountOfMoney -= prize;
        }

        public void SetStartingPosition(int SetX, int SetY)
        {
            x = SetX;
            startingX = x;
            y = SetY;
            startingY = y;
        }

        public double DistanceDriven()
        {
            double distanceDriven = Math.Sqrt(Math.Abs(startingX * startingX - x * x) + Math.Abs(startingY * startingY - y * y));
            return distanceDriven;
        }

        public virtual void Start()
        {
            AwailableToMove = true;
        }


        public virtual void MoveForward(int HowFar)
        {
            if (AwailableToMove)
            {
                y += HowFar * (int)MaxSpeed / 20;
            }else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public virtual void MoveBackward(int HowFar)
        {
            if (AwailableToMove)
            {
                y -= HowFar * (int)MaxSpeed / 20;
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public virtual void MoveRight(int HowFar)
        {
            if (AwailableToMove)
            {
                x += HowFar * (int)MaxSpeed / 20;
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public virtual void MoveLeft(int HowFar)
        {
            if (AwailableToMove)
            {
                x -= HowFar * (int)MaxSpeed / 20;
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public virtual void Stop()
        {
            DistanceCounter = DistanceDriven();
            AwailableToMove = false;
        }

        public void SpeedUp(int HowMuch)
        {
            if (ActualSpeed + HowMuch < MaxSpeed)
                ActualSpeed += HowMuch;
            else if (ActualSpeed < MaxSpeed)
                ActualSpeed = MaxSpeed;
            else Console.WriteLine("You cannot drive faster with this vehicle");
        }

        public void SlowDown(int HowMuch)
        {
            if (ActualSpeed - HowMuch >0)
                ActualSpeed -= HowMuch;
            else if (ActualSpeed > 0)
                ActualSpeed = 0;
            else Console.WriteLine("You have slowed down to 0");
        }

        public override string ToString()
        {
            return VehicleOwner.Name + "\t\t" + modelOfVehicle + "\t" + time;
        }
    }
}
