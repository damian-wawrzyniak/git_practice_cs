﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamianWawrzyniakLab2
{
    public class Owner : Base
    {
        public string Name { get; private set; }
        public int Age { get; private set; }
        public double AmountOfMoney { get; set; }

        public Owner(string setName, int setAge, double money)
        {
            Name = setName;
            Age = setAge;
            AmountOfMoney = money;
        }

    }
}
