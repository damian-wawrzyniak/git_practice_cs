﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamianWawrzyniakLab2
{
    class Car : Vehicle
    {
        public double FuelConsumptionPer100 { get; private set; }
        private double fuelVolume;
        private int fuelMaxVolume;
        private double costsOfDriving = 0;
        public Car(string vehicleModel, int maxVolumeOfFuel, double fuelConsumption) : base(vehicleModel)
        {
            fuelMaxVolume = maxVolumeOfFuel;
            FuelConsumptionPer100 = fuelConsumption;
            fuelVolume = fuelMaxVolume;
        }

        public void Fuel(double fuelPrize, double howMuchFuel)
        {
            if(fuelVolume + howMuchFuel < fuelMaxVolume)
            {
                fuelVolume += howMuchFuel;
                costsOfDriving += howMuchFuel * fuelPrize;
            }else if(fuelVolume < fuelMaxVolume)
            {
                double difference = fuelMaxVolume - fuelVolume;
                fuelVolume = fuelMaxVolume;
                costsOfDriving += difference * fuelPrize;
            }else
            {
                Console.WriteLine("Fuel tank is full");
            }
            time += 10;
        }
        
        public bool FuelTankIsEmpty()
        {
            return fuelVolume == 0;
        }

        private void CheckFuelLevel()
        {
            if (FuelTankIsEmpty()) Fuel(4.0, fuelMaxVolume);
        }

        private int SubtractFuelReturnDistance(int distance)
        {
            if(fuelVolume >= distance * (FuelConsumptionPer100 / 100))
            {
                fuelVolume -= distance * (FuelConsumptionPer100 / 100);
                return distance;
            }
            else
            {
                int fuelLeftFor = (int)(fuelVolume / FuelConsumptionPer100);
                fuelVolume = 0;
                return fuelLeftFor;
            }
        }

        public override void Start()
        {
            time = 0;
            if (!FuelTankIsEmpty())
            {
                AwailableToMove = true;
                fuelVolume -= FuelConsumptionPer100 / 50;
            }
            else Console.WriteLine("Fuel tank is empty");
        }


        public override void MoveForward(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractFuelReturnDistance(distance);
                y += posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckFuelLevel();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }


        public override void MoveBackward(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractFuelReturnDistance(distance);
                y -= posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckFuelLevel();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public override void MoveRight(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractFuelReturnDistance(distance);
                x += posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckFuelLevel();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public override void MoveLeft(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractFuelReturnDistance(distance);
                x += posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckFuelLevel();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public override void Stop()
        {
            DistanceCounter = DistanceDriven();
            VehicleOwner.AmountOfMoney -= costsOfDriving;
            costsOfDriving = 0;
            AwailableToMove = false;
        }

    }
}
