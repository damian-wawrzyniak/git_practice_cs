﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamianWawrzyniakLab2
{
    class Race
    {
        List<Vehicle> raceContestants = new List<Vehicle>();

        public Race()
        {

        }

        public Race(List<Vehicle> vehicleList)
        {
            raceContestants = vehicleList;
        }

        public void AddContestant(Vehicle newVehicle)
        {
            raceContestants.Add(newVehicle);
        }

        public void StartRace(int minimumDistance)
        {
            foreach(Vehicle vehicle in raceContestants)
            {
                vehicle.SetStartingPosition(0, 0);
                vehicle.ActualSpeed = vehicle.MaxSpeed;
                vehicle.Start();
                while(vehicle.DistanceDriven() < minimumDistance)
                {
                    MyRaceTrack(vehicle);
                }
                vehicle.Stop();
            }
        }

        private void MyRaceTrack(Vehicle vehicle)
        {
            vehicle.MoveForward(20);
            vehicle.MoveRight(15);
            vehicle.MoveForward(30);
            vehicle.MoveLeft(10);
            vehicle.MoveForward(25);
        }

        public void DisplayResults()
        {
            Console.WriteLine("Kierowca\tPojazd\tczas");
            foreach (Vehicle vehicle in raceContestants)
            {
                Console.WriteLine(vehicle);
            }
        }
    }
}
