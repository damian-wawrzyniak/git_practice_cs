﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamianWawrzyniakLab2
{
    class Bicycle : Vehicle
    {
        private int numberOfShifters;
        private int maxStaminaOfOwner = 100;
        private int currentStaminaOfOwner;
        private int pointsOfStaminaPer100 = 15;

        public Bicycle(string vehicleModel, int shiftersNumber) : base(vehicleModel)
        {
            time = 0;
            numberOfShifters = shiftersNumber;
            MaxSpeed = numberOfShifters * 3;
            currentStaminaOfOwner = maxStaminaOfOwner;
        }
        public override void ChangeOwnerOfVehicle(Owner newOwner, int prize)
        {
            VehicleOwner = newOwner;
            newOwner.AmountOfMoney -= prize;
            pointsOfStaminaPer100 = newOwner.Age * 5;
        }
        public bool OwnersStaminaIsEmpty()
        {
            return currentStaminaOfOwner == 0;
        }

        public void RestForOwner()
        {
            time += (int) (VehicleOwner.Age * 0.5);
            currentStaminaOfOwner = maxStaminaOfOwner;
            AwailableToMove = true;
        }
        private void CheckOwnersStamina()
        {
            if (OwnersStaminaIsEmpty()) RestForOwner();
        }

        private int SubtractStaminaReturnDistance(int distance)
        {
            if (currentStaminaOfOwner >= distance * (pointsOfStaminaPer100 / 100))
            {
                currentStaminaOfOwner -= distance * (pointsOfStaminaPer100 / 100);
                return distance;
            }
            else
            {
                int staminaLeftFor = (int)(currentStaminaOfOwner / pointsOfStaminaPer100);
                currentStaminaOfOwner = 0;
                return staminaLeftFor;
            }
        }

        public override void Start()
        {
            if (!OwnersStaminaIsEmpty())
            {
                AwailableToMove = true;
            }
            else Console.WriteLine("Fuel tank is empty");
        }


        public override void MoveForward(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractStaminaReturnDistance(distance);
                y += posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckOwnersStamina();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public override void MoveBackward(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractStaminaReturnDistance(distance);
                y -= posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckOwnersStamina();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public override void MoveRight(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractStaminaReturnDistance(distance);
                x += posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckOwnersStamina();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

        public override void MoveLeft(int HowFar)
        {
            if (AwailableToMove)
            {
                int distance = HowFar * (int)ActualSpeed / 20;
                int posibleDistance = SubtractStaminaReturnDistance(distance);
                x += posibleDistance;
                TimeControl(posibleDistance, ActualSpeed);
                CheckOwnersStamina();
            }
            else
            {
                Console.WriteLine("Vehicle is not awailable to move");
            }
        }

    }
}
