﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamianWawrzyniakLab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle toyota = new Car("Toyota", 40, 6.0);
            toyota.MaxSpeed = 120;
            toyota.ChangeOwnerOfVehicle(new Owner("Bartek", 39, 2000), 500);
            Vehicle subaru = new Car("Subaru", 35, 9.0);
            subaru.MaxSpeed = 200;
            subaru.ChangeOwnerOfVehicle(new Owner("Damian", 19, 4000), 1500);
            Vehicle bike = new Bicycle("Rower", 21);
            bike.ChangeOwnerOfVehicle(new Owner("Mateusz", 23, 1200), 350);

            Race myRace = new Race();
            myRace.AddContestant(toyota);
            myRace.AddContestant(subaru);
            myRace.AddContestant(bike);
            myRace.StartRace(100);
            myRace.DisplayResults();
            myRace.StartRace(300);
            myRace.DisplayResults();
            Console.ReadLine();
        }
    }
}
